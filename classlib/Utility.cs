namespace program;
public class Utility
{
    public double GetValueOfBook(Book book)
    {
        return book.Price;
    }
    public double GetValueOfAmulet(Amulet amulet)
    {
        switch (amulet.Quality)
        {
            case (Level.low) : return 12.5;
            case (Level.medium) : return 20.0;
            default : return 27.5;
        }
    }
    public double GetValueOfCourse(Course course)
    {
        int i = 0;
        if (course.DurationInMinutes % 60 != 0)
            i = 1; 
        return ((course.DurationInMinutes - course.DurationInMinutes % 60) / 60 + i) * 875;
    }

}