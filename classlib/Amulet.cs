namespace program;
public class Amulet : Merchandise
{
    public Level Quality {get; set;}
    public string Design {get; set;}
    
    public Amulet (string itemId, Level quality, string design)
    {
        base.ItemId = itemId;
        this.Design = design;
        this.Quality = quality;
    }
    public Amulet (string itemId, Level quality) :
    this (itemId, quality, "")
    {
    }

    public Amulet (string itemId) :
    this (itemId, Level.medium, "")
    {
    }

    public override string ToString()
    {
        return $"ItemId: {ItemId}, Quality: {Quality}, Design: {Design}";
    }
}